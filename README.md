# Shared app resources

Strings and colours live in here so that PRs can be reviewed by both app teams and we retain parity across the apps.

